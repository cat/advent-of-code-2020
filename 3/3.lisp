(defun read-from-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line
          collect line)))

(defvar *testinput* (read-from-file "test.input"))
(defvar *input* (read-from-file "input"))

(defun count-trees (len right down input &optional (curr-right right) (curr-down down))
  (if (null input) 0
      (if (> curr-down 1) (count-trees len right down (rest input) curr-right (- curr-down 1))
          (+ (if (string= #\# (char (car input) curr-right)) 1 0)
             (count-trees len right down (rest input) (mod (+ curr-right right) len) down)))))

(defun solve (input slope-right slope-down)
  (count-trees (length (car input)) slope-right slope-down (rest input)))

;; part1
(print (solve *input* 3 1))

;; part2
(print (reduce #'* (mapcar (lambda (slope) (solve *input* (car slope) (car (last slope))))
               (list '(1 1) '(3 1) '(5 1) '(7 1) '(1 2)))))
