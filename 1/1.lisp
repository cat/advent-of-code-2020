(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line
          collect (parse-integer line))))

(defvar *testinput* (get-file "test.input"))
(defvar *input1* (get-file "input"))

(defun find-2-numbers (input) (remove-if-not (lambda (x) (some (lambda (y) (= 2020 (+ x y))) input)) input))

(defun find-3-numbers (input)
  (remove-if-not
   (lambda (x) (some (lambda (y) (some (lambda (z) (= 2020 (+ x y z))) input)) input))
   input))

(mapcar (lambda (func) (print (reduce #'* (apply func (list *input1*))))) (list #'find-2-numbers #'find-3-numbers))
