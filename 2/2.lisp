(defun parse-line (line)
  (let ((dash-delim (search "-" line)) (colon-delim (search ":" line)) (first-space-delim (search " " line)))
    (list (list (parse-integer (subseq line 0 dash-delim))
                (parse-integer (subseq line (+ 1 dash-delim) first-space-delim)))
          (subseq line (+ 1 first-space-delim) colon-delim)
          (subseq line (+ 2 colon-delim) (length line))
          )))

(defun read-from-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line
          collect (parse-line line))))

(defvar *testinput* (read-from-file "test.input"))
(defvar *input* (read-from-file "input"))

(defun character-total-occurrences (ch str)
  (length (remove-if-not (lambda (x) (string= x ch)) str)))

(defun check1 (limits ch str)
  (let ((ch-count (character-total-occurrences ch str)))
    (and (>= ch-count (car limits)) (<= ch-count (car (last limits))))))

(defun check2 (limits ch str)
  (not (equal (string= ch (char str (- (car limits) 1)))
              (string= ch (char str (- (car (last limits)) 1))))))

(defun validate-password (input func)
  (let* ((limits (pop input)) (ch (pop input)) (str (pop input)))
    (if (apply func (list limits ch str)) 1 0)))

(mapcar (lambda (func)
          (print (reduce #'+ (mapcar (lambda (input) (validate-password input func)) *input*))))
        (list #'check1 #'check2))

